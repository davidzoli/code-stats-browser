(function() {
    "use strict";
	chrome.tabs.onUpdated.addListener(function(tabId, changeInfo, tab) {
		chrome.tabs.executeScript(tabId, {
			file: 'content.js'
		}, () => chrome.runtime.lastError);
	});

	chrome.runtime.onInstalled.addListener(function() {
		if(window.matchMedia('(prefers-color-scheme: dark)').matches) {
			chrome.runtime.sendMessage({scheme:"dark"});
		}
	});
	chrome.runtime.onMessage.addListener(handleMessage);

	function handleMessage(request, sender, sendResponse) {
		if (request.name == "sendXP") {
			var url = "https://codestats.net/api/my/pulses";
	
			fetch(url, {
				method: 'POST',
				headers: {
					"X-API-Token" : request.token,
					"Content-Type" : "application/json"
				},
				body: JSON.stringify(request.data)
			})
				.then((response) => response.json())
				.then((data) => sendResponse(data))
				.catch((error) => console.error( "Error occured: " + error))
			return true;
		} else {
			if ( request.scheme == 'dark' ) {
				chrome.browserAction.setIcon({
					path : {
						"128" : "./assets/icons/128x128_alt.png",
						"48" : "./assets/icons/48x48_alt.png",
						"32" : "./assets/icons/32x32_alt.png",
						"16" : "./assets/icons/16x16_alt.png"
					}
				})
			} else {
				chrome.browserAction.setIcon({
					path : {
						"128" : "./assets/icons/128x128.png",
						"48" : "./assets/icons/48x48.png",
						"32" : "./assets/icons/32x32.png",
						"16" : "./assets/icons/16x16.png"
					}
				})
			}
		}
	  }
})();