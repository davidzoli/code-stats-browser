(function(){
    "use strict";

    var xps = new Map();

    var pulse = setInterval(function() {
        if (xps.size > 0) {
            chrome.storage.local.get(["token"], function(items) {
                if (items.token ) {
                    var json = {
                        "coded_at": rfc3339(new Date()),
                        "xps": [
                        ]
                    }

                    for (const [key,xp] of xps.entries() ) {
                        json.xps.push({"language":key, xp});
                    }
                    
                    chrome.runtime.sendMessage({
                        name: "sendXP",
                        data : json,
                        token : items.token
                    }, handleResponse);
                }
            });
        }
    },10000);

    function handleResponse(response) {
        if (response.error) {
            $('#cs_counter').html("C::S " + ' <span style="color:red">Wrong token!</span>');
            clearInterval(pulse);
        } else {
            xps.clear();
            $('#cs_counter').text("C::S sent");
        }
    }

    $("body").ready(() => {

        /*
            [{PHP: 8}, {JS: 10}]
        */

        function incrementCounter(language) {
            if (xps.has(language)) {
                xps.set(language, xps.get(language) + 1);
            } else {
                xps.set(language, 1);
            }
        }

        setTimeout(function() {
            // ace editor
            if($(".ace_content").length) {
                $('body').append('<div id="cs_counter" style="z-index:9999; font-size:12px; position:fixed;bottom:10px;left:10px;padding:6px;color:#fff;background:rgba(0,0,0,0.8);font-family:sans-serif">C::S ready</div>');
                
                $('body').on('keyup','textarea', function(e) {
                    if ($('.ace_string').text().match(/"ace\/mode\/(.+)"/)) {
                        var language = getLanguage($('.ace_string').text().match(/"ace\/mode\/(.+)"/)[1]);
                    } else {
                        language = getLanguage($('.ace_identifier').text());
                    }

                    var code = $(this).parent().find(".ace_content").text();
                    var codeLen = code.length;

                    if (codeLen > 0) {
                        incrementCounter(language);
                        $('#cs_counter').text(language + ": " + xps.get(language));
                    }
                });
            }

            // monaco editor
            if($(".monaco-editor").length) {
                $('body').append('<div id="cs_counter" style="z-index:9999; font-size:12px; position:fixed;bottom:10px;left:10px;padding:6px;color:#fff;background:rgba(0,0,0,0.8);font-family:sans-serif">C::S 0</div>');
                
                $('body').on('keyup','textarea', function(e) {
                    var language = getLanguage($(".monaco-editor").parent().attr('data-mode-id'));

                    var textAreaLen = $(this).val().length;
                    var code = $(".view-lines").text();
                    var codeLen = code.length;

                    if (textAreaLen != 0 || (codeLen > 1 && e.keyCode == 8)) {
                        incrementCounter(language);
                        $('#cs_counter').text(language + ": " + xps.get(language));
                    }
                });
            }

            // code mirror
            if($(".CodeMirror").length) {
                $('body').append('<div id="cs_counter" style="z-index:9999; font-size:12px; position:fixed;bottom:10px;left:10px;padding:6px;color:#fff;background:rgba(0,0,0,0.8);font-family:sans-serif">C::S 0</div>');
    
                var found = document.documentElement.innerHTML.match(/"?mode"?\s*:\s*"(.+?)"/);
                
                if (found) {
                    $('body').on('keyup','textarea', function(e) {
                        var language = getLanguage(found[1]);
    
                        var textAreaLen = $(this).val().length;
                        var code = $(".CodeMirror-code pre").text();
                        var codeLen = code.length;
    
                        if (textAreaLen != 0 || (codeLen > 1 && e.keyCode == 8)) {
                            incrementCounter(language);
                            $('#cs_counter').text(language + ": " + xps.get(language));
                        }
                    });
                } else {
                    $('body').on('keyup','textarea', function(e) {
                        var found = $(this).parent().parent().attr("class").match(/cm-s-(.*)\s{1}/);
                        var language = getLanguage(found[1]);
                        
                        // Hackerrank SQL fix
                        if (language === 'Plain text' && $('#select-lang').val() === 'mysql') {
                            language = 'mysql';
                        }
    
                        var textAreaLen = $(this).val().length;
                        var code = $(this).parent().parent().find(".CodeMirror-code pre").text();
                        var codeLen = code.length;
    
                        if (textAreaLen != 0 || (codeLen > 1 && e.keyCode == 8)) {
                            incrementCounter(language);
                            $('#cs_counter').text(language + ": " + xps.get(language));
                        }
                    });
                }
            }
        }, 10000);
    });
})();

function getLanguage(language) {
    language = language.toLowerCase();
    if (language.includes("python")) {
        return "Python";
    } else if (language.includes('php')) {
        return "PHP";
    } else if (language.includes("html")) {
        return "HTML";
    } else if (language.includes("mysql")) {
        return "SQL";
    } else if (language.includes("javascript")) {
        return "JavaScript";
    } else if (language.includes("json")) {
        return "JSON";
    } else if (language.includes("css")) {
        return "CSS";
    } else if (language.includes("typescript")) {
        return "Typescript";
    } else if (language.includes("java")) {
        return "Java";
    } else {
        return "Plain text";
    }
}

function rfc3339(d) {
    
    function pad(n) {
        return n < 10 ? "0" + n : n;
    }

    function timezoneOffset(offset) {
        var sign;
        if (offset === 0) {
            return "Z";
        }
        sign = (offset > 0) ? "-" : "+";
        offset = Math.abs(offset);
        return sign + pad(Math.floor(offset / 60)) + ":" + pad(offset % 60);
    }

    return d.getFullYear() + "-" +
        pad(d.getMonth() + 1) + "-" +
        pad(d.getDate()) + "T" +
        pad(d.getHours()) + ":" +
        pad(d.getMinutes()) + ":" +
        pad(d.getSeconds()) + 
        timezoneOffset(d.getTimezoneOffset());
}